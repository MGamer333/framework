<?php 
    require_once '../src/App.php';


$app = new App([
    "default_file_path" => dirname( __DIR__ ) . "/resources/"
]);

    $app->container( function( $container ) {
        $container->default = new App\Controller\DefaultController( $container );
    });

    require_once '../app/routes.php';

$app->run();
<?php 

spl_autoload_register( function( $c ) 
{
  $document_root = dirname( __DIR__, 1 );

  $required = true;
  $file = $document_root . "\\" . $c . ".php";
  $file = str_replace( "\\", "/", $file );

  file_exists( $file ) ? require_once $file : $required = false;

  if ( !$required )
  {
    if ( file_exists( dirname( __DIR__, 2 ) . "/config/config.json" ) )
    {
      $configFile = fopen( dirname( __DIR__, 2 ) . "/config/config.json", "r" );
      $config = json_decode( fread( $configFile, filesize( dirname( __DIR__, 2 ) . "/config/config.json" ) ), true );
      fclose( $configFile );
    }
    else
      throw new Exception( "\"config.json\" does not exists!" );

    if ( !isset( $config["autoload"]["psr-4"] ) )
      return false;

    foreach ( $config["autoload"]["psr-4"] as $key => $value )
    {
      if ( strpos( $c, $key ) !== false )
      {
        $c = str_replace( $key, "", $c );
        $document_root = $value;
        $file = dirname( __DIR__, 2 ) . "\\" . $document_root . "\\" . $c . ".php";
        $file = str_replace( "\\", "/", $file );

        file_exists( $file ) ? require_once $file : $required = false;
        break;
      }
    }
  }
});

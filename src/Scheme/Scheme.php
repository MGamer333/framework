<?php 
    namespace Scheme;

class Scheme
{
    protected $charset = "utf-8";
    protected $title = "Application error";
    protected $messages = [
        404 => "The page you are looking for could not be found. Check the address bar to ensure your URL is spelled correctly.",
        405 => "This page does not support this request method type."
    ];


    protected $baseTemplate = 
        '<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8" />
            <title>%TITLE%</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style>
                * {
                    font-family: sans-serif;
                }

                body {
                    padding: 20px
                }
            </style>
        </head>
        <body>
            <h1 style="margin: 0; font-weight: 300; font-size: 50px">%MESSAGE%</h1>
            <h5 style="margin: 0; font-weight: 300; font-size: 14px">%STATUS%</h5>
        </body>
        </html>';

    protected $template = [
        "head" => '<!DOCTYPE html><html><head><meta charset="utf-8" /><title>%TITLE%</title><meta name="viewport"   content="width=device-width, initial-scale=1"><style>* {font-family: sans-serif;} body {padding: 20px}</style></head><body>',

        "footer" => '</body></html>'
    ];


    public function createApplicationErrorPage( $e, int $status )
    {
        $response = $this->template["head"];
        $response = str_replace( "%TITLE%", "Application error", $response );
        
        $response = $response . '<h1 style="margin: 0; font-weight: 300; font-size: 50px">Application error</h1><h5 style="margin: 5px 0 0 0; font-weight: 300; font-size: 14px">The application could not run because of the following error:</h5><h4 style="font-weight: bold; margin: 25px 0 25px 0">Details</h4>';

        $response = $response . "<div style='font-size: 12px;'><span> <b style='font-weight: bold;'>Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> " . get_class( $e ) . "</span><br  style='margin-bottom: 4px'>
        <span> <b style='font-weight: bold'>Message:&nbsp;&nbsp;&nbsp;</b> {$e->getMessage()}</span><br  style='margin-bottom: 4px'>
        <span> <b style='font-weight: bold'>File:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> {$e->getFile()}</span><br  style='margin-bottom: 4px'>
        <span> <b style='font-weight: bold'>Line:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> {$e->getLine()}</span></div>";

        $response = $response . '<h4 style="font-weight: bold; margin: 30px 0 25px 0">Trace</h4><div style="font-size: 13px; font-family: Helvetica,Arial,Verdana,sans-serif;">';

        $traces = explode( "#", $e->getTraceAsString() );
        foreach ( $traces as $trace )
            if ( trim( $trace ) !== "" )
                $response = $response . "<p style='margin: 5px 0 0 0; font-family: inherit'>#{$trace}</p>";

        $response = str_replace( "%STATUS%", $status, $response );

        return $response;
    }

    public function createErrorPage( string $message, int $status, string $status_message = null )
    {
        $response = $this->template["head"];
        $response = str_replace( "%TITLE%", $message, $response );

        array_key_exists( $status, $this->messages ) ? $rstatus = $this->messages[$status] : $rstatus = "Status: {$status}";
            !is_null( $status_message ) ? $rstatus = $status_message : null;

        $response = $response . '<h1 style="margin: 0; font-weight: 300; font-size: 50px">' . $message . '</h1>
        <h5 style="margin: 0; font-weight: 300; font-size: 14px">' . $rstatus . '</h5>';

        return $response . $this->template["footer"];
    }
}
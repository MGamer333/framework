<?php 
    namespace Http\Request;

class Request 
{
    protected $requestParam = [];

    function __construct()
    {   
        $this->requestParam["Headers"] = [];

        foreach ( $_SERVER as $key => $value )
        {
            if ( substr( $key, 0, 5 ) === "HTTP_" ) 
            {
                $this->requestParam["Headers"][str_replace( " ", "-", ucwords( str_replace( "_", " ", strtolower( substr ( $key, 5 ) ) ) ) )] = $value;
                continue;
            }

            $this->requestParam[str_replace( "_", "", ucwords(strtolower($key), "_") )] = $value;
        }

        $this->requestParam["Uri"] = strpos( $_SERVER["REQUEST_URI"], "?" ) === false ? $_SERVER["REQUEST_URI"] : str_replace( substr( $_SERVER["REQUEST_URI"], strpos( $_SERVER["REQUEST_URI"], "?" ), strlen( $_SERVER["REQUEST_URI"] ) ), "", $_SERVER["REQUEST_URI"] );

        if ( $_SERVER["REQUEST_METHOD"] === "GET" )
            $this->requestParam["Args"] = $_GET;
        elseif ( $_SERVER["REQUEST_METHOD"] === "POST" )
            $this->requestParam["Args"] = $_POST;
        else
            $this->requestParam["Args"] = [];

        $this->requestParam["RequestBody"] = $this;
    }

    function __call( string $function, $params )
    {
        if ( substr( $function, 0, 3 ) === "get" )
            if ( array_key_exists( substr( $function, 3, strlen( $function ) ), $this->requestParam ) )
                return $this->requestParam[substr( $function, 3, strlen( $function ) )];

        throw new Exception\RequestException( "Undefined function \"{$function}\"" );
    }
}
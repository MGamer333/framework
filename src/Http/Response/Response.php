<?php 
    namespace Http\Response;

class Response
{
    protected $status = 200;
    protected $headers = [
        "Content-Type" => "text/html"
    ];
    protected $cookieHeaders = [];
    protected $body = null;
    protected $charset = "UTF-8";


    private $messages = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        226 => 'IM Used',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        421 => 'Misdirected Request',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        444 => 'Connection Closed Without Response',
        451 => 'Unavailable For Legal Reasons',
        499 => 'Client Closed Request',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
        599 => 'Network Connect Timeout Error',
    ];

    function __call( string $function, $param )
    {
        if ( substr( $function, 0, 3 ) === "get" )
            if ( array_key_exists( lcfirst( substr( $function, 3, strlen( $function) ) ), get_object_vars( $this ) ) )
                return $this->{lcfirst( substr( $function, 3, strlen( $function) ) )};
            else
                throw new Exception\ResponseException( "Undefined property: \"{$function}\"!" );
        elseif ( substr( $function, 0, 3 ) === "set" )
            if ( array_key_exists( lcfirst( substr( $function, 3, strlen( $function) ) ), get_object_vars( $this ) ) )
                $this->{lcfirst( substr( $function, 3, strlen( $function ) ) )} = $param[0];
            else
                throw new Exception\ResponseException( "Undefined property: \"{$function}\"!" );
        return $this;
    }

    /* Header */
    public function setHeader( string $name, string $value )
    {
        if ( !array_key_exists( $name, $this->headers ) )
            $this->headers[$name] = $value;
        else
            throw new Exception\ResponseException( "Header \"{$name}\" has already set!" );
    }

    public function modifyHeader( string $name, string $value )
    {
        if ( array_key_exists( $name, $this->headers ) )
            $this->headers[$name] = $value;
        else
            throw new Exception\ResponseException( "Header \"{$name}\" does not exists!" );
    }

    public function hasHeader( string $name )
    {
        if ( array_key_exists( $name, $this->headers ) )
            return $this->headers[$name];
        else
            return false;
    }
    /* Header */

    /* Response methods */
    public function withJson( string $json, int $status = null )
    {
        self::modifyHeader( "Content-Type", "application/json" );
        !is_null( $status ) ? $this->status = $status : null;
        $this->body = $json;

        return $this;
    }

    public function withStatus( int $status, string $message = null )
    {
        $this->status = $status;
        $scheme = new \Scheme\Scheme;
        $this->body = $scheme->createErrorPage( $this->messages[$status], $status, $message );

        return $this;
    }

    public function withBody( string $body, string $type = null, int $status = null )
    {
        $this->body = $body;
        !is_null( $status ) ? $this->status = $status : null;
        !is_null( $type ) ? self::modifyHeader( "Content-Type", $type ) : null;

        return $this;
    }

    public function withRedirect( string $location, int $status = null )
    {
        !is_null( $status ) ? $this->status = $status : $this->status = 301;
        self::setHeader( "Location", $location );

        return $this;
    }

    public function withFile( string $location, inst $status = null )
    {
        !is_null( $status ) ? $this->status = $status : $this->status = $this->status;

        if ( !file_exists( $location ) )
            throw new Exception\ResponseException( "File \"{$location}\" does not exists!" );

        $this->body = file_get_contents( $location );

        return $this;
    }
    /* Response methods */

    /* Special headers */
    public function setCookie( string $name, string $value, string $expire = null, string $path = "/", string $domain = null, bool $httponly = false, bool $secure = false )
    {
        $cookie = "{$name}={$value};";

        !is_null( $expire ) ? $cookie = $cookie . " expires={$expire};" : null;
        $cookie = $cookie . " path={$path};";
        !is_null( $domain ) ? $cookie = $cookie . " domain={$domain};" : null;
        $httponly ? $cookie = $cookie . " HttpOnly;" : null;
        $secure ? $cookie = $cookie . " Secure;" : null;

        array_push( $this->cookieHeaders, $cookie );

        return true;
    }
    /* Special headers */

    public function getStatusMessage( int $status )
    {
        if ( !array_key_exists( $status, $this->messages ) )
            return false;

        return $this->messages[$status];
    }
}
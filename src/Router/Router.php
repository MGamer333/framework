<?php 
    namespace Router;

class Router
{
    protected $request;
    protected $response;
    protected $scheme;
    protected $args;


    function __construct()
    {
        $this->request = new \Http\Request\Request( $this->config );
        $this->response = new \Http\Response\Response;
        $this->scheme = new \Scheme\Scheme;
        $this->args = $this->request->getArgs();
    }

    public function getCurrent()
    {
        if ( $this->request->getRequestMethod() === "OPTIONS" )
            return self::handleCors();

        foreach ( $this->routes as $routes )
        {
            if ( $routes["path"] === $this->request->getUri() )
            {
                if ( is_array( $routes["method"] ) )
                {
                    foreach ( $routes["method"] as $method )
                    {
                        if ( $method === $this->request->getRequestMethod() )
                        {
                            return self::handleCurrent( $routes );
                        }

                        $notAllowed = true;
                    }
                }
                elseif ( is_string( $routes["method"] ) )
                {
                    if ( $routes["method"] === $this->request->getRequestMethod() )
                    {
                        return self::handleCurrent( $routes );
                    }

                    $notAllowed = true;
                }
            }
        }

        if ( isset( $notAllowed ) )
            throw new Exception\RouterException( "Method not allowed \"{$this->request->getRequestMethod()}\"", 405 );

        throw new Exception\RouterException( "Page not found", 404 );
    }

    public function handleCors()
    {
        foreach ( $this->routes as $routes )
        {
            if ( $routes["path"] == $this->request->getUri() )
            {
                if ( is_array( $routes["method"] ) )
                {
                    foreach ( $routes["method"] as $method )
                    {
                        if ( $method === "OPTIONS" )
                        {
                            return self::handleCurrent( $routes );
                        }
                    }
                }
                else
                {
                    if ( $routes["method"] === "OPTIONS" )
                    {
                        return self::handleCurrent( $routes );
                    }
                }
            }
        }

        $response = $this->response;
            $response->setHeader( "Access-Control-Allow-Credentials", $this->defaultCorsHeaders["Access-Control-Allow-Credentials"] );
            $response->setHeader( "Access-Control-Allow-Origin", $this->defaultCorsHeaders["Access-Control-Allow-Origin"] );
            $response->setHeader( "Access-Control-Allow-Methods", $this->defaultCorsHeaders["Access-Control-Allow-Methods"] );
            $response->setHeader( "Access-Control-Allow-Headers", $this->defaultCorsHeaders["Access-Control-Allow-Headers"] );
            $response->setHeader( "Access-Controll-Max-Age", $this->defaultCorsHeaders["Access-Control-Max-Age"] );
        return $response;
    }

    public function handleCurrent( array $route )
    {
        if ( is_callable( $route["handler"] ) )
        {
            $response = $route["handler"]( $this->request, $this->response, $this->args );
        }
        elseif ( is_string( $route["handler"] ) )
        {
            if ( count( explode( ":", $route["handler"] ) ) !== 2 )
                throw new \Exception\AppException( "Wrong handler definition!" );

            $handler = explode( ":", $route["handler"] );
            
            foreach ( get_object_vars( $this->container ) as $key => $value )
            {
                if ( is_object( $value ) )
                    if ( substr( get_class( $value ), 0, 15 ) === "App\Controller\\" )
                        if ( substr( get_class( $value ), 15 ) === $handler[0] )
                            if ( in_array( $handler[1], get_class_methods( $value ) ) )
                            {
                                $response = call_user_func([
                                    $value,
                                    $handler[1]
                                ], $this->request, $this->response, $this->args);

                                break;
                            }
                $response = 404;
            }
        }

        if ( is_int( $response ) && is_string( $route["handler"] ) && $response === 404 )
            throw new \Exception\AppException( "Controller {$route['handler']} does not exists!" );

        if ( !is_object( $response ) && !get_class( $response ) !== "Http\Response\Response" && is_null( $response ) )
            throw new \Exception\AppException( "Route handler must return an instance of \"Http\Response\Response\" !" );

        return $response;
    }

    protected function sendResponse( \Http\Response\Response $response )
    {
        foreach ( $response->getHeaders() as $key => $value )
            header( "{$key}: {$value}" );

        if ( !isset( $response->getHeaders()["Access-Control-Allow-Origin"] ) )
            header( "Access-Control-Allow-Origin: *" );

        foreach ( $response->getCookieHeaders() as $cookie )
            header( "Set-Cookie: {$cookie}" );

        http_response_code( $response->getStatus() );

        echo !is_null( $response->getBody() ) ? $response->getBody() : "";
    }

    protected function sendErrorPage( $e, bool $try = true )
    {
        if ( !is_object( $e ) &&  get_class( $e ) !== "Error" && is_subclass_of( $e, "\Exception" ) === 1 )
            throw new Exception\RouterException( "Argument 1 must be an instance of \\Error or Exception!" );

        $status = method_exists( $e, "getStatus" ) ? $e->getStatus() : 500;

        if ( $try && array_key_exists( $status, $this->errorRoutes ) )
        {
            try 
            {
                return self::sendResponse( 
                    self::handleCurrent([
                    "handler" => $this->errorRoutes[$status]
                    ]) 
                );
            }
            catch ( \Exception $e )
            {
                return self::sendErrorPage( $e, false );
            }
        }

        $response = $this->response;
        
        if ( substr( (string) $status, 0, 1 ) == 5 )
        {
            $body = $this->scheme->createApplicationErrorPage( $e, $status );
            $response->withBody( $body, null, $status );
        }
        else
        {
            $body = $this->scheme->createErrorPage( $this->response->getStatusMessage( $status ), $status );
            $response->withBody( $body, null, $status );
        }

        return self::sendResponse( $response );
    }
}

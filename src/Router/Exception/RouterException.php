<?php 
    namespace Router\Exception;

class RouterException extends \Exception implements \Throwable
{
    protected $status = 500;

    function __construct( string $message = null, string $status = null )
    {
        !is_null( $message ) ? $this->message = $message : null;
        !is_null( $status ) ? $this->status = $status : null;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
<?php
    namespace Exception;

class NoticeException extends \Exception implements \Throwable
{
    function __construct( string $message, string $file, int $line )
    {
        $this->message = $message;
        $this->file = $file;
        $this->line = $line;
    }
}
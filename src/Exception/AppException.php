<?php 
    namespace Exception;

class AppException extends \Exception implements \Throwable
{
    protected $status = 500;
    
    function __construct( string $message, int $status = null )
    {
        !is_null( $message ) ? $this->message = $message : null;
        !is_null( $status ) ? $this->status = $status : null;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
<?php 
    require_once 'Autoload/Autoload.php';

class App extends Router\Router
{
    protected $routes = [];
    protected $errorRoutes = [];
    protected $container;
    protected $config;
    protected $documentRoot;
    protected $defaultCorsHeaders = [
        "Access-Control-Allow-Credentials" => "true",
        "Access-Control-Allow-Origin" => "*",
        "Access-Control-Allow-Methods" => "*",
        "Access-Control-Allow-Headers" => "X-PINGOTHER, Content-Type",
        "Access-Control-Max-Age" => "86400"
    ];
    protected $defaultFilePath;

    private $current;
    

    function __construct( array $constructConfig = [] )
    {
        self::setErrorHandlers();

        parent::__construct();
        $this->container = new \stdClass();
        $this->documentRoot = __DIR__;

        isset( $constructConfig["default_file_path"] ) ? $this->defaultFilePath = $constructConfig["default_file_path"] : null;

        $this->container->filePath = function( string $location ) {
            return $this->defaultFilePath . $location;
        };
    }

    public function route( string $method, string $path, $handler )
    {
        if ( !is_string( $handler ) && !is_callable( $handler )  )
            throw new \Exception\AppException( "Route handler must be a string or callable!" );

        if ( $path[0] === "{" && $path[strlen($path)-1] === "}" )
        {
            $this->errorRoutes[str_replace( ["{", "}"], "", $path )] = $handler;
            return;
        }

        count( explode( "|", $method ) ) === 1 ? null : $method = explode( "|", $method );

        array_push( $this->routes, [
            'method' => $method,
            'path' => $path,
            'handler' => $handler
        ]);
    }

    public function setDefaultCors( string $allow_origin, string $allow_methods, string $allow_headers, string $allow_max_age = "86400", string $allow_credentials = "false" )
    {
        $this->defaultCorsHeaders["Access-Control-Allow-Credentials"] = $allow_credentials;
        $this->defaultCorsHeaders["Access-Control-Allow-Origin"] = $allow_origin;
        $this->defaultCorsHeaders["Access-Control-Allow-Methods"] = $allow_methods;
        $this->defaultCorsHeaders["Access-Control-Allow-Headers"] = $allow_headers;
        $this->defaultCorsHeaders["Access-Control-Max-Age"] = $allow_max_age;
    }

    public function container( callable $callback )
    {
        try 
        {
            $callback( $this->container );
        }
        catch ( Exception $e )
        {
            $this->sendErrorPage( $e );
            die();
        }
    }

    public function run()
    {
        try 
        {
            $response = $this->getCurrent();
        }
        catch( Exception $e )
        {
            $this->sendErrorPage( $e );
            die();
        }

        return $this->sendResponse( $response );
    }

    protected function setErrorHandlers()
    {
        set_exception_handler( function ( $e ) {
            $this->sendErrorPage( $e );
            die();
        });
        
        set_error_handler( function ( $s, $message, $file, $line ) {
            $this->sendErrorPage( new \Exception\NoticeException( $message, $file, $line ) );
            die();
        });
    }
}
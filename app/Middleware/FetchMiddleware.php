<?php 
    namespace App\Middleware;

class FetchMiddleware
{
    protected $payload;
    protected $contentType;

    function __construct()
    {
        $this->contentType = isset( $_SERVER["CONTENT_TYPE"] ) ? trim( $_SERVER["CONTENT_TYPE"] ) : '';
        $this->payload = trim( file_get_contents( "php://input" ) );
    }

    public function getPayload( string $contentType )
    {
        if ( $this->contentType === $contentType )
        {
            return $this->payload;
        }

        return false;
    }

    public function getRawPayload()
    {
        return $this->payload;
    }

    public function getJsonPayload( string $contentType = "application/json" )
    {
        if ( $this->contentType === $contentType )
        {
            try 
            {
                $content = json_decode( $this->payload, true );
            }
            catch( \Exception $e )
            {
                return false;
            }

            return isset( $content ) ? $content : false;
        }

        return false;
    }
}
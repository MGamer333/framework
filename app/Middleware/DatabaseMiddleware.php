<?php 
    namespace App\Middleware;

class DatabaseMiddleware extends \PDO
{
    protected $query;

    function __construct( array $config = [] )
    {
        if ( !file_exists( dirname( __DIR__, 2 ) . "/config/database.json" ) )
            throw new Exception( 'Database config file does not exists!' );

        $config = json_decode( file_get_contents( dirname( __DIR__, 2 ) . "/config/database.json" ), true );

        parent::__construct( "mysql:host={$config['host']};charset=utf8", $config['username'], $config['password'] ); 
    }

    public function createQuery( string $query )
    {
        $this->query = parent::prepare( $query );
            return $this;
    }
    
    public function bindParams( string $name, $value, \PDO $type = null )
    {
        if ( is_null( $type ) )
            $this->query->bindParam( $name, $value );
        else
            $this->query->bindParam( $name, $value, $type );

        return $this;
    }

    public function executeQuery()
    {
        $this->query->execute();
            return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function fetchQuery()
    {
        $returnArray = [];
        while ( $rows = $this->query->fetchAll() )
            foreach ( $rows as $row )
                array_push( $returnArray, $row );

        return $returnArray;
    }
}
<?php 

$app->route( "GET|POST|PUT|DELETE", "/", "DefaultController:home" );

$app->route( "GET", function( $request, $response ) {
    return $response->withJson(json_encode([
        "success" => true
    ]));
});
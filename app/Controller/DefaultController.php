<?php
    namespace App\Controller;

class DefaultController extends Controller 
{
    public function home( $request, $response )
    {
        $response->withStatus( 418 );
        return $response->withFile( ($this->container->filePath)( "home.html" ) );
    }

    public function handleCors( $request, $response )
    {
        $response->setHeader( "Access-Control-Allow-Origin", "*" );
        $response->setHeader( "Access-Control-Allow-Credentials", "true" );
        $response->setHeader( "Access-Control-Allow-Methods", "POST" );
        $response->setHeader( "Access-Control-Allow-Headers", "X-PINGOTHER, Content-Type" );
            return $response;
    }

    public function index( $request, $response )
    {
        
    }
}
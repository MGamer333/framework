<?php 
    namespace App\Controller;

class Controller 
{
    protected $container;
    protected $documentRoot;

    function __construct( $container )
    {
        $this->container = $container;
        $this->documentRoot = dirname( __DIR__, 2 );
    }

    protected function getDocumentRoot()
    {
        return $this->documentRoot;
    }

    protected function getOrigin( $request )
    {
        $origin = isset( $request->getHeaders()["Origin"] ) ? $request->getHeaders()["Origin"] : "*";
        
        if ( strpos( $origin, "http://" ) !== -1 ) $origin = str_replace( "http://", "", $origin );
        if ( strpos( $origin, "https://" ) !== -1 ) $origin = str_replace( "https://", "", $origin );

        return $origin;
    }
}